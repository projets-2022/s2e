Pour générer l'image à partir du fichier `bitfield-payload-lora.clj`: 

1/ se rendre sur https://kroki.io/#try

2/ Sélectionner "Bytefield" pour le type du schéma

3/ Copier-Coller le contenu de `bitfield-payload-lora.clj` dans la zone d'édition