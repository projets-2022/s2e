Pour générer l'image à partir du fichier `flux-logiciels.drawio` :

1/ Se rendre sur https://app.diagrams.net/

2/ Sélectionner "Open Existing diagram"

3/ Exporter l'image depuis le menu "File > Export as..." avec le format de son choix

Note : si format SVG choisi alors veiller à activer l'option "Embed Images" pour intégrer les images bitmaps inclues dans le schéma