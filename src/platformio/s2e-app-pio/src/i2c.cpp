#include <hardwareserial.h>

#include "i2c.h"

I2c& I2c::getInstance(int bus)
{
    static I2c singleton(bus);

    return singleton;
}

void I2c::setClock(unsigned int kHz) {

    m_i2c->setClock(kHz * 1000);

    return;
}

int I2c::write(uint8_t addr, uint8_t value) {
    int ret;
    int err;

    m_i2c->begin();

    m_i2c->beginTransmission(addr);

    ret = m_i2c->write(value);


    err = m_i2c->endTransmission();
    if(err) {
        ret = -1;
        Serial.print("I2C transmission error : ");
        Serial.println(err /*m_i2c->getErrorText(err)*/);
    }

    m_i2c->end();

    return ret;
}

int I2c::write(uint8_t addr, uint8_t * buf, int len) {
    int ret;
    int err;

    m_i2c->begin();

    m_i2c->beginTransmission(addr);

    ret = m_i2c->write(buf, len);

    err = m_i2c->endTransmission();
    if(err) {
        ret = -1;
        Serial.print("I2C transmission error : ");
        Serial.println(m_i2c->getErrorText(err));
    }

    m_i2c->end();

    return ret;
}

int I2c::available() {
    return m_i2c->available();
}

int I2c::read(uint8_t addr, uint8_t * value) {
    int nbBytesRead = 0;

    m_i2c->requestFrom(addr, static_cast<uint8_t>(1));

    nbBytesRead = m_i2c->available();

    return nbBytesRead;
}

int I2c::read(uint8_t addr, uint8_t * buf, uint8_t len) {
    int nbRetries = 0;
    int nbBytesRead = 0;

    m_i2c->begin();

    m_i2c->requestFrom(addr, len);

    //Serial.print("I2c.read() : ");
    while( nbBytesRead < len) {
        if( !m_i2c->available() ) {
            //Serial.print(".");
            delay(10);
            if (++nbRetries > 10) {
                break;
            }
        } else {
            nbRetries = 0;
            *buf = m_i2c->read();
            //Serial.print(*buf, HEX);
            buf++;
            nbBytesRead++;
        }
    }
    //Serial.print("-> octets lus : ");
    //Serial.println(nbBytesRead);

    m_i2c->end();

    return nbBytesRead;
}

I2c::I2c(int bus) {
    Serial.print("I2C Constructor ");
    if (bus == 0) {
        m_i2c = &Wire;
        //m_i2c->begin(/*freq*/100000);
    } else if (bus == 1) {
        m_i2c = &Wire1;
        //m_i2c->begin(/*freq*/100000); // (sda, scl, freq)
    }
    m_i2c->setTimeOut(500); // augmente le timeout par défaut de 5Oms. Voir https://github.com/espressif/arduino-esp32/issues/1395 pour plus de détails
}

I2c::~I2c()
{
}
