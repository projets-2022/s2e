/**
 * Interface pour gestion du bus I2C
 * 
 */
#ifndef _I2C_H_
#define _I2C_H_

#include <Wire.h>

using namespace std;

class I2c {

public:

    void setClock(unsigned int kHz);
    int write(uint8_t addr, uint8_t value);
    int read(uint8_t addr, uint8_t * value);
    int write(uint8_t addr, uint8_t * buf, int len);
    int available();
    int read(uint8_t addr, uint8_t * buf, uint8_t len);

    // Méthode qui retourne le singleton associé au bus I2C
    static I2c& getInstance(int bus = 0);

    // Déclaration d'un destructeur virtuel qui assure que la destruction d'un objet dérivé de II2c DEPUIS un pointeur
    // sur la classe de base (polymorphisme) appelera bien le destructeur de la classe dérivée et pas uniquement celui de
    // la classe de base.
    virtual ~I2c();

private:

    // Rendre inaccessibles les constructeurs de copie/déplacement et opérateurs d'affectation/déplacement
    I2c(const I2c&) = delete;
    //I2c(I2c&&); // = delete;
    I2c& operator=(const I2c&) = delete;
    //I2c& operator=(I2c&&) = delete;

    explicit I2c(int bus);
    TwoWire * m_i2c;    
};
#endif
