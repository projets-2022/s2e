#include <cmath>
#include "LoRaWan_APP.h"
#include "Arduino.h"
#include "sht21.h"

/* OTAA para*/
//uint8_t devEui[] = { 0x78, 0x44, 0x6f, 0x63, 0x6b, 0x2d, 0x30, 0x31}; // xDock-01
uint8_t devEui[] = { 0x78, 0x44, 0x6f, 0x63, 0x6b, 0x2d, 0x30, 0x31}; // xDock-01
uint8_t appEui[] = { 0x78, 0x44, 0x6f, 0x63, 0x6b, 0x41, 0x70, 0x70}; // xDockApp
uint8_t appKey[] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff };

/* ABP para : inutilisé mais doit quand même être déclaré */
uint8_t nwkSKey[ 16 ];
uint8_t appSKey[ 16 ];
uint32_t devAddr;

/*LoraWan channelsmask, default channels 0-7*/ 
uint16_t userChannelsMask[ 6 ]={ 0x00FF,0x0000,0x0000,0x0000,0x0000,0x0000 };

/* Broche d'interruption sur laquelle est relié le détecteur de fumée */
#define INT_PIN GPIO2

/* Ports LoRa sur lesquels seront envoyées les données */
#define PERIODIC_DATA_PORT 1  // T°/%HR/Niveau de charge batterie
#define ASYNC_DATA_PORT 2     // Fumée

// Structure de données qui stocke les données des capteurs avant leur envoi par LoRa
/*
struct{
  uint32_t temperature : 12; //[-2048, +2047] e dizièmes de degrés (<- plage SHT20 =[-400,+1250])
  uint32_t humidity : 7; // [0,127]
  uint32_t battery : 12; // 2500+[0, 2047]=[2500, 4547] (<- 3V < charge batterie LiPo < 4.225V)
  uint32_t smoke : 1; // [0=false, 1=true]
} s2ePayload;
*/
struct {
    int16_t temperature;
    uint8_t humidity;
    uint16_t battery;
    bool smoke;
} s2ePayload;

bool smokeAlarm = false;
bool smokeAlarmChanged = false;

/* Région LoraWan [LORAMAC_REGION_AS923, ..., LORAMAC_REGION_EU433, LORAMAC_REGION_EU868, ..., LORAMAC_REGION_US915] */
/* -> Mettre ACTIVE_REGION pour utiliser celle spécifiée dans le menu "Tools" de l'IDE */
LoRaMacRegion_t loraWanRegion =  LORAMAC_REGION_EU868;

/* Classe du noeud LoRa : [CLASS_A, CLASS_C] */
/* -> Mettre LORAWAN_CLASS pour utiliser celle spécifiée dans le menu "Tools" de l'IDE */
DeviceClass_t  loraWanClass = CLASS_A;

/*the application data transmission duty cycle.  value in [ms].*/
/*For this example, this is the frequency of the device status packets */
uint32_t appTxDutyCycle = 20 * 1000; // 20s 

/* Mode d'activation LoRa : [OTAA->true, ABP->false] */
/* -> Mettre LORAWAN_NETMODE pour utiliser celui spécifié dans le menu "Tools" de l'IDE */
bool overTheAirActivation = true;

/* Activation du mode Adaptative Data Rate (ADR) : [ON->true, OFF->false] */
/* -> Mettre LORAWAN_ADR pour utiliser celle spécifiée dans le menu "Tools" de l'IDE */
bool loraWanAdr = true;

/* set LORAWAN_Net_Reserve ON, the node could save the network info to flash, when node reset not need to join again */
bool keepNet = LORAWAN_NET_RESERVE;

/* Indicates if the node is sending confirmed or unconfirmed messages */
bool isTxConfirmed = LORAWAN_UPLINKMODE;

/* Application port */
uint8_t appPort = PERIODIC_DATA_PORT;

/*!
* Number of trials to transmit the frame, if the LoRaMAC layer did not
* receive an acknowledgment. The MAC performs a datarate adaptation,
* according to the LoRaWAN Specification V1.0.2, chapter 18.4, according
* to the following table:
*
* Transmission nb | Data Rate
* ----------------|-----------
* 1 (first)       | DR
* 2               | DR
* 3               | max(DR-1,0)
* 4               | max(DR-1,0)
* 5               | max(DR-2,0)
* 6               | max(DR-2,0)
* 7               | max(DR-3,0)
* 8               | max(DR-3,0)
*
* Note, that if NbTrials is set to 1 or 2, the MAC will not decrease
* the datarate, in case the LoRaMAC layer did not receive an acknowledgment
*/
uint8_t confirmedNbTrials = 4;



/* Prepares the payload of the frame */
static bool prepareTxFrame( uint8_t port )
{
  int head;
  appPort = port;
  switch (port) {
    case ASYNC_DATA_PORT: // Réveil par interruption
      Serial.println("Envoi début/fin alerte incendie");
      appDataSize = 1;//AppDataSize max value is 64
      appData[0] = smokeAlarm ? 0xff : 0x00; 
      break;
      
    case PERIODIC_DATA_PORT: // Réveil périodique
      Serial.println("Envoi données capteurs");

      double temp, hum;

      Sht21* trhSensor = new Sht21();
      pinMode(Vext, OUTPUT);
      digitalWrite(Vext, LOW);
      delay(50);

      trhSensor->getTRH(temp, hum);

      delay(100);
      digitalWrite(Vext, HIGH);
      delete trhSensor;

      /*
       struct { // champs de bits qui code la température
        int16_t temperature : 13; // Température en dizièmes de degré [-4096...+4095]
        int16_t unit : 2; // codage de l'unité [0:°C, 1:°F, 2:°K]
        int16_t err : 1; // 0 : OK / 1 : NOK
      };
       */
      if (temp == -273.0) {
        s2ePayload.temperature = -2048;
      } else {
        s2ePayload.temperature = static_cast<int16_t>(std::nearbyint(temp * 10));
        Serial.print("temperature : ");
        Serial.println(s2ePayload.temperature);
      }
      
      if (hum == 0) {
        s2ePayload.humidity = 0x00;
      } else {
        s2ePayload.humidity = static_cast<uint8_t>(std::nearbyint(hum));
        Serial.print("humidity : ");
        Serial.println(s2ePayload.humidity);
      }

//      s2ePayload.battery = static_cast<uint16_t>(((getBatteryVoltage() / 2.0)/4096.0)*100);
      s2ePayload.battery = static_cast<uint16_t>(std::nearbyint(getBatteryVoltage())) - 2500;
      Serial.print("battery : ");
      Serial.println(s2ePayload.battery);

      s2ePayload.smoke = smokeAlarm;
      smokeAlarmChanged = false;
      
      // Sérialisation du payload
      uint32_t * pBytePayload = reinterpret_cast<uint32_t *>(appData);

      *pBytePayload = s2ePayload.temperature << 20;
      *pBytePayload |= s2ePayload.humidity << 13;
      *pBytePayload |= s2ePayload.battery << 1;
      *pBytePayload |= s2ePayload.smoke ? 0x00000001 : 0x00000000;

      appDataSize = 4;
      Serial.print("appDataSize : ");

      Serial.println(appDataSize);
      Serial.print("appData : ");
      for(int i = 0; i < appDataSize; i++) {
        Serial.print(", 0x");
        Serial.print(appData[ i ], HEX);
      }
      Serial.println();
      break;
  }
  return true;
}

void smokeISR()
{
  int pin = digitalRead(INT_PIN);  // -+
  delay(10);                       //  |
  if(digitalRead(INT_PIN) == pin)  // -+-> Anti-rebond sommaire
  {
    smokeAlarm = pin == HIGH ? true : false;
    smokeAlarmChanged = true;
  }
}

void setup() {
  boardInitMcu();
  Serial.begin(115200);
#if(AT_SUPPORT)
  enableAt();
#endif
  deviceState = DEVICE_STATE_INIT;
  LoRaWAN.ifskipjoin();

  smokeAlarmChanged = false;
  pinMode(INT_PIN, INPUT);
  attachInterrupt(INT_PIN, smokeISR, BOTH);
  Serial.println("Interrupts attached");
}

void loop()
{
  if (smokeAlarmChanged) {
    uint32_t now = TimerGetCurrentTime();
    Serial.print(now); 
    Serial.print(" Alerte incendie : ");
    Serial.println(digitalRead(INT_PIN)==LOW ? "OFF" : "ON");
  }

  switch( deviceState )
  {
    case DEVICE_STATE_INIT:
    {
#if(AT_SUPPORT)
      getDevParam();
#endif
      printDevParam();
      LoRaWAN.init(loraWanClass,loraWanRegion);
      deviceState = DEVICE_STATE_JOIN;
      break;
    }
    case DEVICE_STATE_JOIN:
    {
      LoRaWAN.join();
      break;
    }
    case DEVICE_STATE_SEND:
    {
      prepareTxFrame( PERIODIC_DATA_PORT );
      LoRaWAN.send();
      deviceState = DEVICE_STATE_CYCLE;
      break;
    }
    case DEVICE_STATE_CYCLE:
    {
      // Schedule next packet transmission
      txDutyCycleTime = appTxDutyCycle + randr( 0, APP_TX_DUTYCYCLE_RND );
      LoRaWAN.cycle(txDutyCycleTime);
      deviceState = DEVICE_STATE_SLEEP;
      break;
    }
    case DEVICE_STATE_SLEEP:
    {
      if (smokeAlarmChanged) {
        if (IsLoRaMacNetworkJoined) {
          if(prepareTxFrame(ASYNC_DATA_PORT)) {
            LoRaWAN.send();
          }
        }
        smokeAlarmChanged = false;
      }
      LoRaWAN.sleep();
      break;
    }
    default:
    {
      deviceState = DEVICE_STATE_INIT;
      break;
    }
  }
}
