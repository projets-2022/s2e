#include <hardwareserial.h>
#include <math.h>

#include "sht21.h"


Sht21::Sht21(int i2cBus) : m_i2c(I2c::getInstance(i2cBus)) {
}

int Sht21::getTRH(double& temp, double& hum) {

    uint16_t tSensor = getMeasurement(Sht21::T);
    if(tSensor != 0xffff)  {
        temp = -46.85 + 175.72 * (tSensor/65536.0); // temperature en °C
    } else {
        temp = -273.0;
    }

    uint16_t hSensor = getMeasurement(Sht21::RH);
    if(hSensor != 0xffff) {
        hum = -6 + 125 * (hSensor/65536.0); // humidité relative en %
    } else {
        hum = 0.0;
    }


    Serial.print("SHT21 - Temperature : ");
    Serial.println(temp);
    Serial.print("SHT21 - Humidite : ");
    Serial.println(hum);
    
    return 0;
}

double Sht21::computeDewPoint(double& temp, double& hum) {
    return 243.04*(log(hum/100)+((17.625*temp)/(243.04+temp)))/(17.625-log(hum/100)-((17.625*temp)/(243.04+temp)));
}

/**
 * Fait l'acquisition de la T° ou de la RH% courante sur le capteur
 * @param[in] type valeur à acquérir (`T` -> T°C, `RH` (ou autre) -> RH%]
 */
uint16_t Sht21::getMeasurement(data_t type) {
    uint16_t data;
    int err;
    uint8_t buffer[3];

    if( type == Sht21::T) {
        m_i2c.write(DEVICE_ADDRESS, TEMP_NO_HOLD_CMD);
    } else {
        m_i2c.write(DEVICE_ADDRESS, RH_NO_HOLD_CMD);        
    }

    delay(75); // delai >22ms pour le temps de mesure de la temperature et delai >29ms pour le temps de mesure de l'humidité. Voir datasheet p.9
   
    if(m_i2c.read(DEVICE_ADDRESS, buffer, 3) == 3) {
        data = (buffer[0] << 8) 
            | (buffer[1] & ~0x03) // RAZ des 2 bits d'état
            ;
    } else {
        Serial.println("data <- 0xffff");
        data = 0xffff;
    }

    return data;
}
