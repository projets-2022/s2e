#ifndef _SHT21_H_
#define _SHT21_H_

#include "i2c.h"
#include "sht21.h"

class Sht21 {
   public:
      Sht21(
         int i2cBus = 0 /**< N° du bus I2C de l'ESP32 utilisé pour communiquer avec le capteur */
         );
      int getTRH(double& temp, double& hum); 
      double computeDewPoint(double& temp, double& hum);      
    private:
      const int DEVICE_ADDRESS = 0x40; /**< Adresse I2C du SHT21 (non configurable) */
      const int TEMP_NO_HOLD_CMD = 0xF3; /**< Commande pour lire T° san clock stretching */
      const int RH_NO_HOLD_CMD = 0xF5; /**< Commande pour lire RH% sans clock stretching */
      const uint8_t FRAME_LENGTH = 3;  /**< Longueur trame pour T° et RH% (codées sur 2 octets + 1 octet de checksum) */
      enum data_t {T, RH};
      uint16_t getMeasurement(data_t type);
      I2c& m_i2c; /**< objet pour communiquer sur l'I2C (singleton) */
};

#endif