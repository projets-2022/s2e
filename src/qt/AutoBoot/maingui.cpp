#include "maingui.h"
#include "ui_maingui.h"

#include <QSerialPortInfo>
#include <QThread>

MainGui::MainGui(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainGui)
{
    ui->setupUi(this);
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos) {
        ui->cbxCommPorts->addItem(info.portName());
    }

    serial.setBaudRate(QSerialPort::Baud115200);
    serial.setDataBits(QSerialPort::Data8);
    serial.setParity(QSerialPort::NoParity);
    serial.setStopBits(QSerialPort::OneStop);
    serial.setFlowControl(QSerialPort::NoFlowControl);

    connect(&serial, &QSerialPort::readyRead, this, &MainGui::onReadyRead);
}

MainGui::~MainGui()
{
    delete ui;
}


void MainGui::on_btnOpen_clicked()
{
    if( ui->btnOpen->isChecked()) {
        serial.setPortName(ui->cbxCommPorts->currentText());
        serial.open(QSerialPort::ReadWrite);

        ui->btnOpen->setText("Close");
        ui->grpbxAutoBootLogic->setEnabled(true);

        updateDtrRtsState();
    } else {
        updateDtrRtsState();

        serial.close();

        ui->btnOpen->setText("Open");
        ui->grpbxAutoBootLogic->setEnabled(false);
    }
}

void MainGui::onReadyRead()
{
    ui->tedtData->moveCursor(QTextCursor::End);
    ui->tedtData->insertPlainText(serial.readAll());
}


void MainGui::on_btnClear_clicked()
{
    ui->tedtData->clear();
}


void MainGui::on_rbtnDtrHigh_clicked()
{
    if(serial.isOpen()) {
        serial.setDataTerminalReady(false);
        updateDtrRtsState();
    }
}


void MainGui::on_rbtnDtrLow_clicked()
{
    if(serial.isOpen()) {
        serial.setDataTerminalReady(true);
        updateDtrRtsState();
    }
}


void MainGui::on_rbtnRtsHigh_clicked()
{
    if(serial.isOpen()) {
        serial.setRequestToSend(false);
        updateDtrRtsState();
    }
}


void MainGui::on_rbtnRtsLow_clicked()
{
    if(serial.isOpen()) {
        serial.setRequestToSend(true);
        updateDtrRtsState();
    }
}


void MainGui::on_btnBootLoaderMode_clicked()
{
    serial.setDataTerminalReady(false); //high
    serial.setRequestToSend(false); //high

    // laisse passer le reset à 0
    serial.setRequestToSend(true); //low
    serial.setDataTerminalReady(true); //low

    // relâche le reset
    serial.setRequestToSend(false); // high
    serial.setDataTerminalReady(true);

    QThread::msleep(10);
    serial.setDataTerminalReady(false);
}

void MainGui::on_btnReset_clicked()
{
    serial.setDataTerminalReady(false);
    serial.setRequestToSend(true);
    serial.setRequestToSend(false);
}

void MainGui::updateDtrRtsState()
{
    if(serial.isDataTerminalReady()) {
        ui->rbtnDtrLow->setChecked(true);
    } else {
        ui->rbtnDtrHigh->setChecked(true);
    }

    if(serial.isRequestToSend()) {
        ui->rbtnRtsLow->setChecked(true);
    } else {
        ui->rbtnRtsHigh->setChecked(true);
    }

}
