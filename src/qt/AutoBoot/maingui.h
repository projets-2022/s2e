#ifndef MAINGUI_H
#define MAINGUI_H

#include <QDialog>
#include <QSerialPort>

QT_BEGIN_NAMESPACE
namespace Ui { class MainGui; }
QT_END_NAMESPACE

class MainGui : public QDialog
{
    Q_OBJECT

public:
    MainGui(QWidget *parent = nullptr);
    ~MainGui();

private slots:
    void on_btnOpen_clicked();
    void onReadyRead();

    void on_btnClear_clicked();

    void on_rbtnDtrHigh_clicked();

    void on_rbtnDtrLow_clicked();

    void on_rbtnRtsHigh_clicked();

    void on_rbtnRtsLow_clicked();

    void on_btnBootLoaderMode_clicked();

    void on_btnReset_clicked();

private:
    Ui::MainGui *ui;
    QSerialPort serial;
    void updateDtrRtsState();
};
#endif // MAINGUI_H
