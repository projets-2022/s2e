import QtQuick 2.11

Item {
    id: battery
    width: 128
    height: 96

    property var metaData : ["value", "unit",
        "textColor"]

    property int value;
    property color textColor: Qt.rgba(0.03, 0.3, 0.5, 1) //inner text color
    property string unit: "%"
    property int fontSize: height / 3

    function update(value) {
        battery.value = value;
        if (value >= 20 ) {
            chargeLevel.color = "limegreen";
        } else if ((value <= 15) && (value > 10)){
            chargeLevel.color = "goldenrod";
        } else {
            chargeLevel.color = "firebrick";
        }
    }

    Column {
        id: column
        anchors.fill: parent
        spacing: 5


        Item {
            id: element1
            //width:128
            //height: 52
            width: parent.width
            height: parent.height

            Image {
                id: image
                x: 0
                y: 0
                z: 1
                width: parent.width
                fillMode: Image.PreserveAspectFit
                source: "my-battery.svg"
                sourceSize.width: battery.width
            }

            Rectangle {
                id: chargeLevel
                color: "limegreen"
                //height: 45
                height: parent.height - 5
                width : battery.value * (parent.width - 12)/100
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.verticalCenter: parent.verticalCenter
            }

            Row {
                id: row
                z: 2
                spacing: 5
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            Text {
                id: label
                text: battery.value
                font.bold: true
                font.pointSize: battery.fontSize
                color: battery.textColor
            }
            Text {
                id: unit
                text: battery.unit
                color: battery.textColor
                font.pointSize: battery.fontSize / 2
                font.bold: true
                anchors.verticalCenter: label.verticalCenter
            }
          }
        }
    }
}



/*##^##
Designer {
    D{i:4;anchors_width:100}D{i:1;anchors_height:400;anchors_width:200}
}
##^##*/
