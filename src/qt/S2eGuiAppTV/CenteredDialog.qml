// CenteredDialog.qml
import QtQuick 2.11
import QtQuick.Controls 2.4

Dialog {
    parent: ApplicationWindow.overlay

    x: (parent.width - width) / 2
    y: (parent.height - height) / 2

    focus: true
    modal: true

    property alias text: messageText.text

    Label {
        id: messageText

        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter

        anchors.fill: parent
    }

    standardButtons: Dialog.Ok
}
