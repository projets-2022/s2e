import QtQuick 2.11
import QtQuick.Layouts 1.11

//all the automatic meagurements are made from height of Item
Item {
    id: knob
    transformOrigin: Item.Center

    property var metaData : ["from", "to", "value", "unit",
        "reverse",
        "fromAngle", "toAngle",
        "lineWidth", "fontSize",
        "knobBackgroundColor", "knobColor", "textColor",
        "title", "titleFont", "titleFontSize", "titleFontColor"]

    //value parameters
    property double from:0
    property double value: 1
    property double to: 100
    property string unit: "°C"

    property color textColor: Qt.rgba(0.03, 0.3, 0.5, 1) //inner text color

    //progress from right to left
    property bool reverse: false

    //progress circle angle
    property double fromAngle: Math.PI - 1
    property double toAngle: Math.PI *2 + 1

    property int lineWidth: height / 10
    property int fontSize: height / 7

    property color knobBackgroundColor: Qt.rgba(0.1, 0.1, 0.1, 0.1)
    property color knobColor: Qt.rgba(0.2, 0.62, 0.93, 0.7)

    property string title: ""
    property alias titleFont: labelTitle.font.family
    property alias titleFontSize: labelTitle.font.pointSize
    property alias titleFontColor: labelTitle.color

    function update(value) {
        knob.value = value
        canvas.requestPaint()
        background.requestPaint()
        label.text = value;
    }

    Text {
        id: labelTitle
        y: 0
        text: knob.title
        color: Qt.rgba(0, 0, 0., 0.5)
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Canvas {
        id: background
        width: parent.height
        height: parent.height
        antialiasing: true

        property int radius: background.width/2

        onPaint: {

            if (knob.title !== "") {
                background.y = labelTitle.height
                background.x = labelTitle.height/2
                background.height = parent.height - labelTitle.height/2
                background.width = parent.height - labelTitle.height/2
                background.radius = background.height /2
            }

            var centreX = background.width / 2.0;
            var centreY = background.height / 2.0;

            var ctx = background.getContext('2d');
            ctx.strokeStyle = knob.knobBackgroundColor;
            ctx.lineWidth = knob.lineWidth;
            ctx.lineCap = "round"
            ctx.beginPath();
            ctx.clearRect(0, 0, background.width, background.height);
            ctx.arc(centreX, centreY, radius - knob.lineWidth, knob.fromAngle, knob.toAngle, false);
            ctx.stroke();
        }
    }

    Canvas {
        id:canvas
        width: parent.height
        height: parent.height
        antialiasing: true

        property double step: knob.value / (knob.to - knob.from) * (knob.toAngle - knob.fromAngle)
        property int radius: height/2

        /*
        Text {
            id: label
            width: 40
            height: 12
            font.bold: true
            z: 1
            font.pointSize: knob.fontSize
            text: knob.value.toFixed(2)
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.fill: parent
        }
        */

        ColumnLayout {
            id: columnLayout
            z: 1
            spacing: 6
            anchors.fill: parent

            Text {
                id: label
                font.bold: true
                font.pointSize: knob.fontSize
                text: knob.value.toFixed(2)
                color: knob.textColor
                Layout.fillHeight: false
                //transformOrigin: Item.Center
                Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            }
            Text {
                id: unit
                text: knob.unit
                color: knob.textColor
                font.pointSize: knob.fontSize / 2
                font.bold: true
                Layout.fillHeight: false
                Layout.fillWidth: false
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            }


        }

        Image {
            id: image
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.height / 2
            height: parent.height /2
            fillMode: Image.PreserveAspectFit
            source: "dew.svg"
            visible: true
            opacity: 0.5
        }

        onPaint: {

            if (knob.title !== "") {
                canvas.y = labelTitle.height
                canvas.x = labelTitle.height/2
                canvas.height = parent.height - labelTitle.height/2
                canvas.width = parent.height - labelTitle.height/2
                canvas.radius = canvas.height /2
            }

            var centreX = canvas.width / 2.0;
            var centreY = canvas.height / 2.0;

            var ctx = canvas.getContext('2d');
            //ctx.drawImage('dew.svg', 100, 10)
            ctx.strokeStyle = knob.knobColor;
            ctx.lineWidth = knob.lineWidth;
            ctx.lineCap = "round"
            ctx.beginPath();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            if (knob.reverse) {
                ctx.arc(centreX, centreY, radius - knob.lineWidth, knob.toAngle, knob.toAngle - step, true);
            } else {
                ctx.arc(centreX, centreY, radius - knob.lineWidth, knob.fromAngle, knob.fromAngle + step, false);
            }
            ctx.stroke();
        }

        //Component.onCompleted: {
        //   loadImage("dew.svg")
        //}
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
