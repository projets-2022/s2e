#include <algorithm>
#include <QDebug>

#include "datas.h"

Datas::Datas(QObject *parent) : QObject(parent)
{

    _currIdx = 0;
    _subscription = nullptr;
    _mqttClient = nullptr;
    _brokerPort = 0;
    _brokerURL = nullptr;
    _displayDelay = 0;

    // Définir le format et le chemin vers le fichier de paramétrage :
    // C:\Users\<user>\AppData\Roaming\Crossdock\S2eGuiApp.ini
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "Crossdock", "S2eGuiApp");
    settings.setIniCodec("UTF-8");
    //qDebug() << ".ini file path : " << _settings->fileName();

    // Construire les structures de données JSON associées à chaque capteur à partir du fichier de paramétrage
    QStringList grps = settings.childGroups();

    struct Sensor {
        //int idx; // Pas nécessaire car la structure des clés d'un tableau fait qu'elles seront triées automatiquement
        QString devEUI;
        QString location;
        //bool operator<(const Sensor& other) const {
        //    return idx < other.idx; // tri par index
        //}
    };
    QVector<Sensor> sensors;

    // Créer une configuration initiale artificielle si le fichier .ini n'existe pas
    if(grps.length() == 0) {
        sensors.clear();

        // Spécification des capteurs
        sensors.push_back({"00-00-00-00-00-00-00-00", "Zone capteur n°1"});
        sensors.push_back({"11-11-11-11-11-11-11-11", "Zone capteur n°2"});
        sensors.push_back({"22-22-22-22-22-22-22-22", "Zone capteur n°3"});

        settings.beginWriteArray("sensors");
        for (int i = 0; i < sensors.size(); ++i) {
            settings.setArrayIndex(i);
            settings.setValue("deveui", sensors.at(i).devEUI);
            settings.setValue("location", sensors.at(i).location);
        }
        settings.endArray();

        // Spécification du broker MQTT
        settings.beginGroup("mqtt-broker");
        settings.setValue("url", "broker.emqx.io");
        settings.setValue("port", 1883);
        settings.endGroup();

        // Spécification du délai d'affichage entre chaque écran
        settings.beginGroup("display");
        settings.setValue("delay", 5000);
        settings.endGroup();

        // Ecriture de la config dans le fichier .ini
        settings.sync();
    }

    // Traitement des paramètres de la config

    // 1/ Config des capteurs
    int size = settings.beginReadArray("sensors");
    sensors.clear();
    for (int i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        Sensor aSensor;
        aSensor.devEUI = settings.value("deveui").toString();
        aSensor.location = settings.value("location").toString();
        //aSensor.idx = settings.value("idx").toInt();
        sensors.push_back(aSensor);
    }
    settings.endArray();

    //std::sort(sensors.begin(), sensors.end());

    for( auto elt : sensors) {
        QJsonObject jobj;
        jobj.insert("chipID", elt.devEUI);
        jobj.insert("location", elt.location);
        _datas.append(jobj);

        onMqttMsgReceived(
                    "{ \"_dt\": 0, \"temperature\": 0.0, \"humidity\": 0, \"dewpoint\": 0, \"battery\": 0,\"smoke\": false }"
                    , QMqttTopicName(_SENSORS_DATA_TOPIC + "/" + elt.devEUI)
                    );
    }

    bool ok;

    // 2/ config du broker
    _brokerURL = settings.value("mqtt-broker/url", "broker.emqx.io/").toString();
    _brokerPort = settings.value("mqtt-broker/port", 1883).toInt(&ok);
    if(!ok) {
        _brokerPort = 1883;
    }

    // 3/ config du délai d'affichage entre écrans
    int delay = settings.value("display/delay", 5000).toInt(&ok);
    if(!ok) {
        delay = 5000;
    }
    setDisplayDelay(delay);


    _mqttClient = new QMqttClient();

    connect(_mqttClient, &QMqttClient::connected, this, &Datas::onMqttConnected);
    connect(_mqttClient, &QMqttClient::messageReceived, this, &Datas::onMqttMsgReceived);

    _mqttClient->setHostname(_brokerURL);
    _mqttClient->setPort(_brokerPort);
    _mqttClient->connectToHost();

}

Datas::~Datas()
{
    delete _mqttClient;
}

QJsonObject Datas::getNextData()
{
    QJsonObject data;

    data = _datas[ _currIdx ];

    _currIdx = (_currIdx + 1) % _datas.size();

    return data;
}

int Datas::getPercentBattery(int batLevel)
{
    int percent;
/*
    Données provenant de https://www.reddit.com/r/ElectricalEngineering/comments/c9w50a/trying_to_show_lipo_battery_charge_as_a_percentage/
    4,2     100
    4,15	95
    4,11	90
    4,08	85
    4,02	80
    3,98	75
    3,95	70
    3,91	65
    3,87	60
    3,85	55
    3,84	50
    3,82	45
    3,8     40
    3,79	35
    3,77	30
    3,75	25
    3,73	20
    3,71	15
    3,69	10
    3,61	5
    3,27	0
*/
    if (batLevel >= 4200) {
        percent = 100;
    } else if (batLevel >= 4150) {
         percent = 95;
    } else if (batLevel >= 4110) {
        percent = 90;
    } else if (batLevel >= 4080) {
        percent = 85;
    } else if (batLevel >= 4020) {
        percent = 80;
    } else if (batLevel >= 3980) {
        percent = 75;
    } else if (batLevel >= 3950) {
        percent = 70;
    } else if (batLevel >= 3910) {
        percent = 65;
    } else if (batLevel >= 3870) {
        percent = 60;
    } else if (batLevel >= 3850) {
        percent = 55;
    } else if (batLevel >= 3840) {
        percent = 50;
    } else if (batLevel >= 3820) {
        percent = 45;
    } else if (batLevel >= 3800) {
        percent = 40;
    } else if (batLevel >= 3790) {
        percent = 35;
    } else if (batLevel >= 3770) {
        percent = 30;
    } else if (batLevel >= 3750) {
        percent = 25;
    } else if (batLevel >= 3730) {
        percent = 20;
    } else if (batLevel >= 3710) {
        percent = 15;
    } else if (batLevel >= 3690) {
        percent = 10;
    } else if (batLevel >= 3610) {
        percent = 5;
    } else {
        percent = 0;
    }

    return percent;
}

int Datas::getDisplayDelay() {
    return _displayDelay;
}

void Datas::setDisplayDelay(int theDelay) {
    _displayDelay = theDelay;
}

void Datas::onMqttConnected()
{
    QMqttTopicFilter topicFilter = QMqttTopicFilter(_SENSORS_TOPIC);

    qDebug() << "topicFilter.isValid() : " << topicFilter.isValid();

    if ((! _subscription) // pas de souscription déjà existante
            && topicFilter.isValid() // nom du topic valide
            && (_subscription = _mqttClient->subscribe(topicFilter)) // souscription réussie
            ) {
        qDebug() << "SUCCESS : Subscription to sensors topic";
    } else {
        qDebug() << "ERROR : Subscription to sensors topic";
    }
}

void Datas::onMqttMsgReceived(const QByteArray &message, const QMqttTopicName &topic)
{
    QJsonParseError jsonErr;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(message, &jsonErr);

    qDebug() << "jsonErr : " << jsonErr.errorString();

    if( jsonDoc.isObject()) {
        // Parcourir les structure de données des capteurs pour trouver celle correspondant
        // à celui venant d'émettre ses valeurs
        bool found = false;
        QJsonObject * sensor = nullptr;
        for(int i = 0; i < _datas.size(); i++) {
            if (_datas[ i ].value("chipID") == topic.levels().at(2)) {
                found = true;
                sensor = &_datas[ i ];
                break;
            }
        }
        // Compléter la structure de données du capteur avec toutes les informations transmises :
        // _dt -> timestamp,
        // + temperature/humidity/dewpoint/battery/smoke SI topic "crossdock/data/<chipID>",
        // OU
        // + smoke SI topic "crossdock/alert/<chipID>"
        if(found) {
            QJsonObject jobj = jsonDoc.object();
            QStringList keys = jobj.keys();
            for(int i = 0; i < keys.size(); i++) {
                sensor->insert(keys[ i ], jobj.take(keys[ i ]));
            }
            qDebug() << "SUCCESS : new JSON object read from topic value";
        }
    } else {
         qDebug() << "ERROR : topic value is not a JSON object !";
    }
}
