#ifndef DATAS_H
#define DATAS_H

#include <QObject>
#include <QtMqtt>

class Datas : public QObject
{
    Q_OBJECT
public:
    explicit Datas(QObject *parent = nullptr);
    ~Datas();

signals:

public:
    Q_INVOKABLE QJsonObject getNextData();
    Q_INVOKABLE int getDisplayDelay();
    Q_INVOKABLE int getPercentBattery(int batLevel);
    void setDisplayDelay(int theDelay);

private:
    int _currIdx;
    int _displayDelay;
    QMqttClient * _mqttClient;
    QMqttSubscription * _subscription;
    QString _brokerURL;
    int _brokerPort;
    const QString _SENSORS_TOPIC = "crossdock/#";
    const QString _SENSORS_DATA_TOPIC = "crossdock/data";
    const QString _SENSORS_ALERT_TOPIC = "crossdock/alert";
    QList<QJsonObject> _datas;

private slots:
    void onMqttConnected();
    void onMqttMsgReceived(const QByteArray &message, const QMqttTopicName &topic);
};

#endif // DATAS_H
