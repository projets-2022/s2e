import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12

import "qrc:/"

import tech.lyceebenoit.s2e 1.0

Window {
    id: dashboard
    //width: 1920
    //height: 1080
    visible: true
    //1920
    //1080
    color: "#ffffff"
    title: qsTr("S2eGuiApp")

    Component.onCompleted: {
            dashboard.showFullScreen();
    }

    CenteredDialog {
        id: centeredDialog
        property bool isFired : false
    }

    S2eDatas {
        id: sensors
    }

    Timer {
        interval: sensors.getDisplayDelay()
        triggeredOnStart: true
        running: true
        repeat: true

        onTriggered: {
            //imgFireAlarm.visible = !imgFireAlarm.visible
            //rlaySensors.visible = !rlaySensors.visible
            let jobj = sensors.getNextData();
            console.log("jobj : " + Object.keys(jobj));
            if(jobj.smoke) {
                imgFireAlarm.visible = true;
                rlaySensors.visible = false;
            } else {
                imgFireAlarm.visible = false;
                rlaySensors.visible = true;
                waterLevel.update(jobj.humidity);
                knob.update(jobj.temperature);
                dewpoint.update(jobj.dewpoint);
                batteryGauge.update(sensors.getPercentBattery(jobj.battery))
            }

            txtLieu.text = jobj.location;

            var date = new Date(jobj._dt);
            txtDateTime.text = date.toLocaleDateString(Qt.locale("fr_FR"), "ddd dd MMM ''yy") // Jeud. 01 janv. '70
                    + " - "
                    + date.toLocaleTimeString(Qt.locale("fr_FR"), "hh:mm:ss");

            console.log("jobj.chipID : " + jobj.chipID);
            if(jobj.chipID === "00-00-00-00-00-00-00-00" && centeredDialog.isFired === false) {
                centeredDialog.title = qsTr("Avertissement !")
                centeredDialog.text = qsTr("Fichier de configuration non renseigné\n")
                        + qsTr("Configuration fictive créée.\n")

                        + qsTr(Qt.platform.os === "windows"
                               ? "Voir [...]\\<user>\\AppData\\Roaming\\Crossdock\\S2eGuiApp.ini\n"
                               : "Voir /home/<user>/.config/Crossdock/S2eGuiApp.ini\n")
                        + qsTr("Redémarrer l'application après avoir renseigné la configuration")
                centeredDialog.visible = true
                centeredDialog.isFired = true
            }
        }
    }

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent
        visible: true

        Text {
            id: txtLieu
            text: qsTr("Lieu")
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.topMargin: 20
            Layout.leftMargin: 50
            font.capitalization: Font.SmallCaps
            font.bold: true
            font.family: "Verdana"
            fontSizeMode: Text.Fit
            font.pixelSize: 40
        }

        Item {
            id: element3
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom

            Image {
                id: imgFireAlarm
                smooth: true
                antialiasing: false
                visible: false
                layer.enabled: false
                source: "fire-alarm.svg"
                sourceSize.height: parent.height // Evite pixelisation
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
            }

            RowLayout {
                id: rlaySensors
                anchors.fill: parent
                transformOrigin: Item.Center
                clip: false
                visible: true
                spacing: 6
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                ColumnLayout {
                    id: columnLayout1
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                    Text {
                        id: element1
                        text: qsTr("Température")
                        font.bold: true
                        font.capitalization: Font.SmallCaps
                        font.family: "Verdana"
                        font.pixelSize: 24
                        Layout.alignment: Qt.AlignHCenter
                    }

                    Knob {
                        id: knob
                        width: 400
                        height: 400
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        from:0
                        to:100
                        value: 50

                    }
                }

                ColumnLayout {
                    id: columnLayout3
                    spacing: 12
                    Layout.fillHeight: false
                    Layout.fillWidth: false
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                    Text {
                        id: element4
                        text: qsTr("Charge batterie")
                        font.bold: true
                        font.capitalization: Font.SmallCaps
                        font.family: "Verdana"
                        font.pixelSize: 16
                        Layout.alignment: Qt.AlignHCenter

                    }

                    BatteryGauge {
                        id: batteryGauge
                        width: 256
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillHeight: false
                        Layout.fillWidth: false
                    }

                    Text {
                        id: element
                        text: qsTr("Point de rosée")
                        font.bold: true
                        font.capitalization: Font.SmallCaps
                        font.family: "Verdana"
                        font.pixelSize: 16
                        Layout.alignment: Qt.AlignHCenter
                    }

                    Dewpoint {
                        id: dewpoint
                        width : 256
                        height: 256
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        value: 50
                    }







                }

                ColumnLayout {
                    id: columnLayout2
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                    Text {
                        id: element2
                        text: qsTr("Humidité")
                        font.bold: true
                        font.capitalization: Font.SmallCaps
                        font.family: "Verdana"
                        font.pixelSize: 24
                        Layout.alignment: Qt.AlignHCenter
                    }

                    WaterLevel {
                        id: waterLevel
                        unit : "%"
                        width: 400
                        height: 400
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        from:0
                        to:100
                        value: 50
                    }
                }



            }

        }

        Text {
            id: txtDateTime
            text: qsTr("Text")
            style: Text.Normal
            font.weight: Font.Bold
            font.capitalization: Font.SmallCaps
            font.family: "Verdana"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            font.pixelSize: 40
        }
    }
}






