
#include <QDebug>

#include "appihm.h"
#include "ui_appihm.h"
#include "editihm.h"

AppIHM::AppIHM(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::AppIHM)
{
    ui->setupUi(this);

    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos) {
        ui->cbxSerialPorts->addItem(info.portName());
    }
    ui->ledtData->setInputMask("HH HH HH HH HH HH HH HH HH HH");
    ui->ledtData->setPlaceholderText("<Type spaces separated hex numbers>");
}

AppIHM::~AppIHM()
{
    delete ui;
}


void AppIHM::on_btnConnect_clicked()
{
    if(ui->btnConnect->isChecked()) {
        ui->cbxSerialPorts->setEnabled(false);
        ui->btnConnect->setText("Disconnect");
        ui->btnEditAppEui->setEnabled(true);
        ui->btnEditAppKey->setEnabled(true);
        ui->btnEditDevEui->setEnabled(true);
        ui->btnEditDutyCycle->setEnabled(true);
        ui->btnRefresh->setEnabled(true);

        connect(ui->ledtDevEui, &QLineEdit::textChanged, this, &AppIHM::onTextChanged);
        connect(ui->ledtAppEui, &QLineEdit::textChanged, this, &AppIHM::onTextChanged);
        connect(ui->ledtAppKey, &QLineEdit::textChanged, this, &AppIHM::onTextChanged);
        connect(ui->ledtDutyCycle, &QLineEdit::textChanged, this, &AppIHM::onTextChanged);

        _htcc = new Cubecell(ui->cbxSerialPorts->currentText());

        connect(_htcc, &Cubecell::chipIdReady, this, &AppIHM::onChipIdReady);
        connect(_htcc, &Cubecell::devEuiReady, this, &AppIHM::onDevEuiReady);
        connect(_htcc, &Cubecell::appEuiReady, this, &AppIHM::onAppEuiReady);
        connect(_htcc, &Cubecell::appKeyReady, this, &AppIHM::onAppKeyReady);
        connect(_htcc, &Cubecell::dutyCycleReady, this, &AppIHM::onDutyCycleReady);
        connect(_htcc, &Cubecell::joinReady, this, &AppIHM::onJoinReady);
        connect(_htcc, &Cubecell::sendHexReady, this, &AppIHM::onSendHexReady);

        _htcc->wakeUp();
        _htcc->getChipID();
        _htcc->getDevEui();
        _htcc->getAppEui();
        _htcc->getAppKey();
        _htcc->getDutyCycle();
    } else {
        ui->cbxSerialPorts->setEnabled(true);
        ui->btnConnect->setText("Connect");
        ui->btnEditAppEui->setEnabled(false);
        ui->btnEditAppKey->setEnabled(false);
        ui->btnEditDevEui->setEnabled(false);
        ui->btnEditDutyCycle->setEnabled(false);
        ui->btnRefresh->setEnabled(false);
        ui->btnApply->setEnabled(false);

        delete _htcc;
    }
}

void AppIHM::onChipIdReady(const QString &s)
{
    _chipID = s;
    ui->ledtChipID->setText(s);
}

void AppIHM::onDevEuiReady(const QString &s)
{
    _devEui = s;
    ui->ledtDevEui->setText(s);
}

void AppIHM::onAppEuiReady(const QString &s)
{
    _appEui = s;
    ui->ledtAppEui->setText(s);
}

void AppIHM::onAppKeyReady(const QString &s)
{
    _appKey = s;
    ui->ledtAppKey->setText(s);
}

void AppIHM::onDutyCycleReady(const QString &s)
{
    ui->ledtDutyCycle->setText(s);  
}

void AppIHM::onJoinReady(const QString &s)
{
    if(s == "joined") {
        ui->lblJoinStatus->setPixmap(QString::fromUtf8(":/img/ok.png"));
    } else {
        ui->lblJoinStatus->setPixmap(QString::fromUtf8(":/img/nok.png"));
    }
}

void AppIHM::onSendHexReady(const QString &s)
{
    ui->tedtLog->append(s);
}


void AppIHM::on_btnEditDevEui_clicked()
{
    if( ui->btnEditDevEui->isChecked()) {
        ui->btnEditDevEui->setText("Cancel");
        ui->ledtDevEui->setReadOnly(false);
        ui->ledtDevEui->clear();
        ui->ledtDevEui->setPlaceholderText(_devEui.isNull() ? "<Enter DevEUI>" : _devEui);
        ui->ledtDevEui->setStyleSheet("color: red;");
    } else {
        ui->btnEditDevEui->setText("Edit");
        ui->ledtDevEui->setReadOnly(true);
        ui->ledtDevEui->setStyleSheet("");
        if( !ui->btnEditDevEui->isChecked()
                && !ui->btnEditAppEui->isChecked()
                && !ui->btnEditAppKey->isChecked()
                && !ui->btnEditDutyCycle->isChecked()
                ) {
            ui->btnApply->setEnabled(false);
        }

        _htcc->wakeUp();
        _htcc->getDevEui();
    }
}


void AppIHM::on_btnEditAppEui_clicked()
{
    EditIhm edit;

    edit.setModal(true);

    edit.exec();

}


void AppIHM::on_btnEditAppKey_clicked()
{
    EditIhm edit;

    edit.setModal(true);

    edit.exec();

}


void AppIHM::on_btnEditDutyCycle_clicked()
{
    EditIhm edit;

    edit.setModal(true);

    edit.exec();

}


void AppIHM::on_btnApply_clicked()
{
    ui->ledtDevEui->setStyleSheet("");
    ui->ledtAppEui->setStyleSheet("");
    ui->ledtAppKey->setStyleSheet("");
    ui->ledtDutyCycle->setStyleSheet("");

    EditIhm edit;

    edit.setModal(true);

    edit.exec();

}

void AppIHM::onTextChanged()
{
    QObject * sigSrc = sender();

    if(sigSrc == ui->ledtDevEui) {
        qDebug() << "DevEUI modifié";
    }

    if( ui->btnEditDevEui->isChecked()
            || ui->btnEditAppEui->isChecked()
            || ui->btnEditAppKey->isChecked()
            || ui->btnEditDutyCycle->isChecked()
            ) {
        ui->btnApply->setEnabled(true);
    }
}


void AppIHM::on_btnRefresh_clicked()
{
    ui->ledtChipID->clear();
    ui->ledtDevEui->clear();
    ui->ledtAppEui->clear();
    ui->ledtAppKey->clear();
    ui->ledtDutyCycle->clear();

    _htcc->wakeUp();
    _htcc->getChipID();
    _htcc->getDevEui();
    _htcc->getAppEui();
    _htcc->getAppKey();
    _htcc->getDutyCycle();
}


void AppIHM::on_btnJoin_clicked()
{
    ui->lblJoinStatus->setPixmap(QString::fromUtf8(":/img/loading.png"));
    _htcc->join();

}

void AppIHM::on_btnSend_clicked()
{
    _htcc->sendHex(ui->ledtData->text());
}
