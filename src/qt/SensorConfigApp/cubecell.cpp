#include <QDebug>
#include <QRegularExpression>

#include "cubecell.h"

Cubecell::Cubecell(QString serialPort, QObject *parent)
    : QObject(parent), _portName(serialPort), _workerThread(serialPort)
{
    connect(&_workerThread, &CubecellWorker::response, this, &Cubecell::onResponseReceived, Qt::ConnectionType::BlockingQueuedConnection);
}

void Cubecell::wakeUp()
{
    _workerThread.transaction(CubecellWorker::WAKE_UP, "");
}

void Cubecell::getChipID()
{
    _workerThread.transaction(CubecellWorker::CHIP_ID, nullptr);
}

void Cubecell::getDevEui()
{
    _workerThread.transaction(CubecellWorker::DEV_EUI, nullptr);
}

void Cubecell::getAppEui()
{
    _workerThread.transaction(CubecellWorker::APP_EUI, nullptr);

}

void Cubecell::getAppKey()
{
    _workerThread.transaction(CubecellWorker::APP_KEY, nullptr);

}

void Cubecell::getDutyCycle()
{
    _workerThread.transaction(CubecellWorker::DUTY_CYCLE, nullptr);

}

void Cubecell::join()
{
    _workerThread.transaction(CubecellWorker::JOIN, "1");
    _workerThread.transaction(CubecellWorker::WAIT_JOIN, "");
}

void Cubecell::sendHex(QString data)
{
    data = data.toUpper(); // Convertit en majuscule
    data.remove(QChar(' ')); // Retire les espaces
    _workerThread.transaction(CubecellWorker::SEND_HEX, data);
    _workerThread.transaction(CubecellWorker::WAIT_SEND_HEX, "");

}

/**
 * @brief Cubecell::hasHexDigits
 * @param srcStr
 * @param hexStr
 * @param len
 * @return
 *
 * Cherche et extrait la 1ère chaine hexadécimale  de 'len' digits trouvée dans 'srcStr'
 */
bool Cubecell::hasHexDigits(const QString &srcStr, QString &hexStr, int len)
{
    QRegularExpression matcher;
    QRegularExpressionMatch match;

    // Motif pour chercher 'len' digits hexadecimaux consécutifs
    matcher.setPattern(tr("[0-9A-F]{%1}").arg(QString::number(len)));

    // Met en place une recherche insensible à la casse
    matcher.setPatternOptions(QRegularExpression::CaseInsensitiveOption);

    // Effectue la recherche
    match = matcher.match(srcStr);

    // Extrait et retourne la chaine si trouvée
    if (match.hasMatch())
    {
        hexStr = match.captured();
        return true;
    }
    return false;
}

bool Cubecell::hasDecDigits(const QString &srcStr, QString &decStr)
{
    QRegularExpression matcher;
    QRegularExpressionMatch match;

    // Motif pour chercher 'len' digits décimaux consécutifs
    matcher.setPattern("[0-9]+");

    // Met en place une recherche insensible à la casse
    matcher.setPatternOptions(QRegularExpression::CaseInsensitiveOption);

    // Effectue la recherche
    match = matcher.match(srcStr);

    // Extrait et retourne la chaine si trouvée
    if (match.hasMatch())
    {
        decStr = match.captured();
        return true;
    }
    return false;

}

void Cubecell::onResponseReceived(const int &idCmd, const QString &s)
{
    //bool isAlreadyWaked = s.indexOf("Waked") != -1 ? false : true;
    bool isAlreadyWaked = true;

    QString response;
    QString hexStr;
    QString decStr;

    qDebug() << "Response cmd<" << idCmd << "> -> " << s;
    if (isAlreadyWaked) {
        switch (idCmd) {
        case ChipID :
            response = s.mid(s.indexOf("+ChipID:"));
            if( hasHexDigits(response, hexStr, 12) ) {
                qDebug() << "ChipID : " << hexStr;
                emit chipIdReady(hexStr);
            }
            break;

        case DevEui :
            response = s.mid(s.indexOf("+DevEui:"));
            if( hasHexDigits(response, hexStr, 16) ) {
                qDebug() << "DevEui : " << hexStr;
                emit devEuiReady(hexStr);
            }
            break;

        case AppEui :
            response = s.mid(s.indexOf("+AppEui:"));
            if( hasHexDigits(response, hexStr, 16) ) {
                qDebug() << "AppEui : " << hexStr;
                emit appEuiReady(hexStr);
            }
            break;


        case AppKey :
            response = s.mid(s.indexOf("+AppKey:"));
            if( hasHexDigits(response, hexStr, 32) ) {
                qDebug() << "AppKey : " << hexStr;
                emit appKeyReady(hexStr);
            }
            break;


        case DutyCycle :
            response = s.mid(s.indexOf("+DutyCycle:"));
            if( hasDecDigits(response, decStr) ) {
                qDebug() << "DutyCycle : " << decStr;
                emit dutyCycleReady(decStr);
            }
            break;

        case WaitJoin :
            if(s.indexOf("joined") != -1)
            {
                emit joinReady("joined");
            } else {
                emit joinReady("not joined");
            }
            break;

        case WaitSendHex :
            emit sendHexReady(s);
            break;

        }
    } else {
        //_workerThread.transaction(CubecellWorker::WAKE_UP, "");
    }
}
