#include <QDebug>
#include <QTime>

#include "cubecellworker.h"

CubecellWorker::CubecellWorker(QString portName, QObject *parent)
    : QThread(parent), _serialPortName(portName)
{

}

CubecellWorker::~CubecellWorker()
{
    _mutex.lock();
    qDebug() << "CubecelWorker destructor begin";

    // Stopper le thread :
    // 1/ demander à sortir de la boucle du thread
    _quit = true;
    // 2/ réveiller le thread pour prendre en compte la demande
    _cond.wakeOne();
    _mutex.unlock();

    // Attendre la fin d'exécution du thread
    wait();
    qDebug() << "CubecellWorker destructor end";

}

void CubecellWorker::transaction(cmd_id_t idCmd, const QString &param)
{
    const QMutexLocker locker(&_mutex);

    _msgQueue.enqueue(qMakePair(idCmd, param));

    qDebug() << "Transaction request : " << _cmds[ idCmd ];

    if (!isRunning()) {
        qDebug() << "Thread started";
        start();
    } else {
        if(_msgQueue.size() == 1) {
            qDebug() << "Thread waked up";
            _cond.wakeOne();
        }
    }
}

void CubecellWorker::run()
{
    QSerialPort serial;

    _mutex.lock();
    serial.setPortName(_serialPortName);
    _mutex.unlock();

    serial.setBaudRate(QSerialPort::Baud115200);
    serial.setDataBits(QSerialPort::Data8);
    serial.setParity(QSerialPort::NoParity);
    serial.setStopBits(QSerialPort::OneStop);
    serial.setFlowControl(QSerialPort::NoFlowControl);

    while( !_quit ) {

        QPair<cmd_id_t, QString> cmd;

        _mutex.lock();
        if( !_msgQueue.empty() ) {
            cmd = _msgQueue.dequeue();

            QByteArray request;
            if( !(cmd.second).isNull() ) {
                request = (_cmds[ cmd.first ] + cmd.second).toLatin1();
            } else {
                request = (_cmds[ cmd.first ] + "?").toLatin1();
            }

            _mutex.unlock();

            qDebug() << "serial isopen : " << serial.isOpen();
            if ( !serial.isOpen() ) {
                if( !serial.open(QIODevice::ReadWrite) ) {
                    qDebug() << "serial open error : " << serial.errorString();
                } else {
                    QSerialPort::PinoutSignals pins = serial.pinoutSignals();
                    qDebug() << "pins : " << pins;

                    serial.setDataTerminalReady(false);
                    pins = serial.pinoutSignals();
                    qDebug() << "pins : " << pins;

                    serial.setRequestToSend(true);
                    pins = serial.pinoutSignals();
                    qDebug() << "pins : " << pins;

                    serial.setRequestToSend(false);
                    pins = serial.pinoutSignals();
                    qDebug() << "pins : " << pins;

                    /*
                    if( serial.waitForReadyRead(_CMD_RD_TIMEOUT)) {
                        QByteArray ATR = serial.readAll();
                        while (serial.waitForReadyRead(_CHUNK_TIMEOUT)) {
                            ATR += serial.readAll();
                        }
                        qDebug() << QString("ATR at %1 : ").arg(QTime::currentTime().toString()) << ATR;
                    } else {
                        qDebug() << "No ATR received";
                    }
                    */
                }
            }

            // SI requête non-vide ALORS l'envoyer
            if( !request.isEmpty() && (serial.write(request) == -1)) {
                qDebug() << "serial write error : " << serial.errorString();
            }

            if (request.isEmpty() || serial.waitForBytesWritten(_CMD_WR_TIMEOUT)) {
                qDebug() << "transaction " << QString(request) << QString("emitted at %1").arg(QTime::currentTime().toString());
                // read response
                if (serial.waitForReadyRead(_CMD_RD_TIMEOUT)) {
                    QByteArray responseData = serial.readAll();
                    qDebug() << QString("incoming data chunk at %1 : ").arg(QTime::currentTime().toString()) << responseData;
                    while (serial.waitForReadyRead(_CHUNK_TIMEOUT))
                        responseData += serial.readAll();

                    const QString incomingData = QString::fromLatin1(responseData);

                    qDebug() << QString("incoming data returned at %1 : ").arg(QTime::currentTime().toString()) << incomingData;
                    emit response(cmd.first, incomingData);
                } else {
                    qDebug() << tr("Wait read response timeout %1").arg(QTime::currentTime().toString());
                    emit timeout(tr("Wait read response timeout %1")
                                 .arg(QTime::currentTime().toString()));
                }
            } else {
                qDebug() << tr("Wait write request timeout %1").arg(QTime::currentTime().toString());
                emit timeout(tr("Wait write request timeout %1")
                             .arg(QTime::currentTime().toString()));
            }
        } else { // Attendre nouvelle transaction
            serial.close();

            qDebug() << "Thread blocked";

            // Relâche le mutex et attent la condition de reveil (_cond.wakeOne())
            // Le mutex doit être verrouillé par le thread appelant avant appel de cette méthode
            // => c'est le cas ici : il est verrouillé dans la méthode transaction()
              _cond.wait(&_mutex);
            qDebug() << "Thread restarted";
            _mutex.unlock();
        }
        //_mutex.lock();
    }
}
