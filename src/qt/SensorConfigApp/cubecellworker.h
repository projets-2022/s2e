#ifndef CUBECELLWORKER_H
#define CUBECELLWORKER_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QQueue>
#include <QPair>
#include <QSerialPort>

class CubecellWorker : public QThread
{
    Q_OBJECT
public:
    explicit CubecellWorker(QString portName, QObject *parent = nullptr);
    ~CubecellWorker();
    typedef enum {WAKE_UP, CHIP_ID, DEV_EUI, APP_EUI, APP_KEY, DUTY_CYCLE, JOIN, WAIT_JOIN, SEND_HEX, WAIT_SEND_HEX} cmd_id_t;
    void transaction(cmd_id_t idCmd, const QString &param);

signals:
    void response(const int &id, const QString &s);
    void error(const QString &s);
    void timeout(const QString &s);

private:
    void run() override;

    QMutex _mutex;
    QWaitCondition _cond;
    bool _quit = false;
    QString _serialPortName;

    QQueue<QPair<cmd_id_t, QString>> _msgQueue;
    const QStringList _cmds = {
      "AT+XXX", "AT+ChipID=", "AT+DevEui=", "AT+AppEui=", "AT+AppKey=", "AT+DutyCycle=", "AT+Join=", "", "AT+SendHex=", ""
    };
    const int _CMD_WR_TIMEOUT = 1000; // 1s
    const int _CMD_RD_TIMEOUT = 10000; // 10s
    const int _CHUNK_TIMEOUT = 50; // 50ms
};

#endif // CUBECELLWORKER_H
