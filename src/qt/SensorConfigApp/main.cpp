#include "appihm.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    AppIHM w;
    w.show();
    return a.exec();
}
