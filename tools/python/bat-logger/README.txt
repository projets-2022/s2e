1/ Enregistrement des payloads MQTT dans un fichier
 screen -dmS S2E-BATT-MONITOR; screen -S S2E-BATT-MONITOR -X stuff $'mosquitto_sub -h broker.emqx.io -p 1883 -t "crossdock/#" -F "%J" > s2e-battery.log\n'

Note : 
* lancer mosquitto_sub via screen permet de laisser le programme s'ex�cuter m�me si on quitte la session Linux
* source du forum ayant permis d'�laborer la commande : https://unix.stackexchange.com/questions/257845/how-to-run-a-program-in-a-screen-redirect-all-output-to-a-file-and-detach

2/ Extraction des donn�es int�ressantes (timestamp et tension batterie) avec `s2e-bat-extract.sh > s2e-bat-data.txt`

3/ Trac� de la courbe des points contenus dans s2e-bat-data.txt avec `bat-plot.py`