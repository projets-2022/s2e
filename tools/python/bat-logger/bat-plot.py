import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime

datas = pd.read_csv('s2e-bat-data.txt')

datas['_dt'] = datas['_dt'].apply(lambda ts: datetime.datetime.fromtimestamp(ts/1000))
datas.set_index("_dt")

# Moyenner les données par heure pour avoir moins de points à tracer
# (on peut mettre "2H", "12H" etc... pour moyenner toutes les 2 heures, 12 heures etc...)
# Source : https://towardsdatascience.com/how-to-group-data-by-different-time-intervals-using-python-pandas-eb7134f9b9b0[How to group data by time intervals in Python Pandas?]
filtered_datas = pd.DataFrame(datas.resample('H', on="_dt").battery.mean())

x = filtered_datas.index
y = filtered_datas.iloc[:]['battery']

plt.figure()

plt.plot(x,y,label='tension batterie en mV',marker='o')
plt.legend()

plt.show()